


Carlife::Application.routes.draw do

  #API's
  get "api/parts/:id" => "vehicles#get_vehicle_parts"
  get "api/popular/:brand" => "vehicles#get_popular_models"
  get "api/vehicle/:user_id" => "client#get_vehicles"

  post "/products/search" => "products#search"


  post "/addmantencion/:vehicle_id" => "maintenances#create"

  post "getpartinfo" => "maintenances#getpartinfo"


  get "getparts" => "maintenances#getparts"

  post "maintenances/create_issue" => "maintenance_issues#create"

  resources :maintenance_issues

  resources :maintenances

  get "clients/new"=>"client#new"
  get "suppliers/new"=>"supplier#new"
  get "clients/edit" => "client#edit"  
  get "clients/:id" => "client#index", as: "client_home"
  get "clients/:id" => "client#index", as: "client"
  get "clients/:id/:vehicle" => "client#index", as: "client_home_vehicle"  
  put "clients/:id" => "client#update"
  get "suppliers/:id" => "supplier#index", as: "supplier_home"
  get "messages" => "messages#index", as: "messages"
  post "messages/create" => "messages#create_history"
  post "messages/compose" => "messages#create_compose"

  get "logout" => "home#logout"
  post "signup/fb" => "client#create_fb"
  post "login/fb" => "home#do_login_fb"
  get "login" => "home#show_login"
  get "login" => "home#show_login"
  post "do_login" => "home#do_login"
  get "admin"=> "home#admin"
  post "clients"=>"client#create"
  post "suppliers"=>"supplier#create"
  get "tests"=>"home#test"
  get "image-search/v1"=>"home#test"
  get "event/delete/:id"=>"events#destroy"
  get "products/delete/:id"=>"products#destroy"
  get "products/edit/:id"=>"products#edit"
  post "vehicles/load-models" => "vehicles#load_models"

  get "quienes_somos" => "home#quienes_somos"
  get "faq" => "home#faq"

  #get "events/new/:id" => "events#new", as: "events_new"
 #post "/clients" => "users#create"


  resources :users
  resources :models
  resources :vehicles
  resources :events
  resources :products
  resources :categories
  resources :parts
  resources :news



  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'


  root :to => 'home#index'
end
