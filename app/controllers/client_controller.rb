# encoding: UTF-8
class ClientController < ApplicationController


  def index
    @client = Client.find(params[:id])
    @owner = (@client == current_user)
    @vehicles = Vehicle.find_all_by_user_id(@client.id)
    @parts_view = !params[:parts].nil?
    @notice = params[:notice]

    if params[:vehicle].nil?
      @selected_vehicle = @vehicles.first
    else
      @selected_vehicle = Vehicle.where(:id => params[:vehicle], :user_id => @client.id).first
    end

  end

  def create
    @client =  Client.new(params[:client])
    @client.type = "Client"


    respond_to do |format|
      if @client.save
        session[:user_id] = @client.id
        format.html { redirect_to new_vehicle_path( {:first =>""}) }
        format.json { render json: @client, status: :created, location: @client }
      else
        format.html { render action: "new" }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end


  def create_fb

    fbemail = params[:username]+"@facebook.com"


    if Client.exists?(:email => fbemail)
  
      #render :js => "if(confirm('Usuario #{params[:first_name]} #{params[:last_name]} ya está registrado en CarLife\\nDebe cerrar sesión para registrar usuario nuevo\\n¿Desea Continuar?')) FB.logout()"
      

      render :js => "signup_existing"
      return
    end

    @client = Client.new(:email => fbemail,:password => "fb_auth",:first_name => params[:first_name], :last_name => params[:last_name], :birth_date => Time.now, :picture => "http://graph.facebook.com/"+params[:username]+"/picture?width=200&height=200" )
    @client.type = "Client"

   respond_to do |format|
      if @client.save
        session[:user_id] = @client.id
      #format.html { redirect_to new_vehicle_path( {:first =>""}) }
       # format.json { render :js => "window.location = '/vehicles/new?first=';console.debug(22222)" }
       
        format.json {render :js => "signup_accepted"}
      else
        format.html { render action: "new" }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end


  def new
    @client = Client.new


    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @client }
    end
  end



  def edit
    @client = current_user


    @gravatar_user = @client.picture.include?("gravatar.com") ? @client.picture.partition('&mail=').last : ""
    @internet_url = (!@client.picture.include?("gravatar.com") && !@client.picture.include?("graph.facebook.com") && !@client.picture.include?("no_pic_user.png") && (@client.picture.length > 0) ) ? @client.picture : ""
    
  end

  def update
    @client = User.find(params[:id])
    respond_to do |format|
      if @client.update_attributes(params[:client])
        format.html { redirect_to @client, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end


  

  def get_vehicles

    @vehiclex=Vehicle.find_all_by_user_id(params[:user_id])

    respond_to do |format|
      format.json { render :file => "client/vehicles.json.erb", :content_type => 'json' }
    end

  end





end
