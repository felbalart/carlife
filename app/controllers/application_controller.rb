class ApplicationController < ActionController::Base
  protect_from_forgery

  private

    def current_user
      User.find(session[:user_id])
    rescue ActiveRecord::RecordNotFound
      nil
    end
    helper_method :current_user


  def owns_vehicle(vid)
    Vehicle.find(vid).user == current_user
  rescue ActiveRecord::RecordNotFound
    false
  end


  def base_components
    bc = Array.new
    ComponentType.all.each do |ctype|
      bc.push(Component.find_or_create_by_name(ctype.name))
    end
    helper_method :base_components
  end

  def estimate_lifetime(grade)

    (365*(3*grade+rand())).to_i

  end


  #before_filter :deficient_parts_warning
  def deficient_parts_warning
    if !current_user.nil?  && current_user.class == Client
      current_user.parts_warn
    end
  end





end