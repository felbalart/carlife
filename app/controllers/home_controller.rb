class HomeController < ApplicationController


  def index

    @user = current_user
    @other_news = News.all.sort_by {|m| m.date}.reverse[0..3]
    @news = @other_news.first
    @other_news.delete(@news)
    @product = Product.all.sample
    @events= Event.all.sort_by(&:created_at).reverse[0..8]


  end


  def logout
    session[:user_id] = nil
    redirect_to root_path
  end


  def show_login
    render "login"
  end

  def do_login

    ok = false
    email = params[:email]
    pass = params[:password]

    user = nil

    if User.exists?(:email => email)
      user = User.find_by_email(email)
      if user.password == pass
        ok = true
      else
      @warning = "Error: Contrasena Incorrecta!"
      end
    else
      @warning = "Error: Email de Usuario No Registrado!"
    end

    if ok
      session[:user_id] = user.id

      if user.type=="Admin"
           redirect_to "/admin"
           return
      else

      if user.type=="Supplier"
          redirect_to "/suppliers/"+user.id.to_s
          return
      end


      redirect_to root_path
      end


    else

    render "login"
    end
  end


  def do_login_fb

    fbemail = params[:username]+"@facebook.com"

    if !Client.exists?(:email => fbemail)
      render :js => "login_unexisting"
      return
    end


    user = User.find_by_email(fbemail)
    session[:user_id] = user.id
    render :js => "login_accepted"


  end

  def admin

    @user=current_user


      if @user == nil
      then
        redirect_to root_path
        return
      end

      if @user.type==nil
        then
        redirect_to root_path
        return
      end

       if @user.type!="Admin"
       then
         redirect_to root_path
         return
       end

  end

  def quienes_somos
  end

  def faq
  end


  def test
    @user = current_user
    @o1 = owns_vehicle(1)
    @o8 = owns_vehicle(8)
    @o52 = owns_vehicle(52)
  end


end
