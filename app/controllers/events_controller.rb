class EventsController < ApplicationController


  def new
    @event = Event.new

    if !owns_vehicle(params[:vehicle])
      redirect_to client_home_path(current_user)
      return
    end

    @vehicle = Vehicle.find(params[:vehicle])

  end

  def create

    event_params = params[:event].except(:type)
    @event =  Event.new(event_params)
    @event.type = params[:event][:type]

    respond_to do |format|
      if @event.save
        format.html { redirect_to client_home_path({:id =>current_user.id,  :vehicle => @event.vehicle})}
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end



  def destroy

    event = Event.find(params[:id])
    vehicle = event.vehicle

    if !owns_vehicle(vehicle)
      redirect_to client_home_path(current_user)
      return
    end

    event.destroy

    respond_to do |format|
      format.html { redirect_to client_home_path({:id =>current_user.id,  :vehicle =>  vehicle})}
      format.json { head :no_content }
    end

  end
end