class MaintenancesController < ApplicationController
  # GET /maintenances
  # GET /maintenances.json
  def index
    @maintenances = Maintenance.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @maintenances }
    end
  end

  # GET /maintenances/1
  # GET /maintenances/1.json
  def show
    @maintenance = Maintenance.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @maintenance }
    end
  end

  # GET /maintenances/new
  # GET /maintenances/new.json
  def new
    @maintenance = Maintenance.new
    @selected_vehicle=params[:vehicle]
    @maintenance.vehicle_id=@selected_vehicle
    @maintenance.date=Time.now
    @maintenance.created_at=Time.now
    @maintenance.updated_at=Time.now
    @maintenance.fixed_parts=0
    @maintenance.changed_parts=0


    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @maintenance }
    end
  end

  # GET /maintenances/1/edit
  def edit
    @maintenance = Maintenance.find(params[:id])
  end

  # POST /maintenances
  # POST /maintenances.json
  def create
    #Falta checkear que las piezas no sean las mismas
    contador=0
    nombre_pieza="part"+contador.to_s



















    if !params[:part0].blank?
    arreglo_piezas=Array.new
    arreglo_piezas<<params[:part0]
    counter=1
    while true
        auxiliar="part"+counter.to_s
        if !params[auxiliar].blank?
            arreglo_piezas<<params[auxiliar]
            counter=counter+1
        else
          break
        end
    end

    if arreglo_piezas.length!=arreglo_piezas.uniq.length

      redirect_to new_maintenance_path({ :vehicle =>  params[:vehicle_id], :error=>"true"})
      return
    end


      mantencion=Maintenance.new
      mantencion.date=Date.today
      mantencion.price = params[:maitenance_price]
      mantencion.vehicle_id=params[:vehicle_id]
      mantencion.changed_parts=0
      mantencion.fixed_parts=0
      mantencion.save


              quedan_piezas=true
              while quedan_piezas

                      if !params[nombre_pieza].blank?

                        maintenanceissue=MaintenanceIssue.new()
                        maintenanceissue.maintenance=mantencion
                        maintenanceissue.fixed=true

                        vehicle=Vehicle.find_by_id(params[:vehicle_id])
                        part=Part.first
                        vehicle.parts.each do |p|
                            if p.component.name==params[nombre_pieza]
                              part=p;
                            end
                        end

                        maintenanceissue.old_grade=part.grade
                        nombre_grade="grade"+contador.to_s
                        maintenanceissue.new_grade=params[nombre_grade]
                        part.grade=params[nombre_grade]

                        maintenanceissue.part=part

                        part.save
                        maintenanceissue.save

                           contador=contador+1
                           nombre_pieza="part"+contador.to_s
                      else
                          quedan_piezas=false
                      end

              end

      mantencion.fixed_parts=contador
      mantencion.save

    end

    respond_to do |format|
      format.html { redirect_to client_home_path({:id =>current_user.id,  :vehicle => params[:vehicle_id], :parts => ""})}
    end

  end

  # PUT /maintenances/1
  # PUT /maintenances/1.json
  def update
    @maintenance = Maintenance.find(params[:id])

    respond_to do |format|
      if @maintenance.update_attributes(params[:maintenance])
        format.html { redirect_to @maintenance, notice: 'Maintenance was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @maintenance.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /maintenances/1
  # DELETE /maintenances/1.json
  def destroy
    @maintenance = Maintenance.find(params[:id])
    @maintenance.destroy

    respond_to do |format|
      format.html { redirect_to maintenances_url }
      format.json { head :no_content }
    end
  end

  def getparts

    @box_id=params[:box_id]
    @parts=Part.find_all_by_vehicle_id(params[:vehicle])
    @selected_vehicle=params[:vehicle]
    @grades=[1,2,3,4,5]
    respond_to do |format|
      format.js
    end

  end

  def getpartinfo
    @parts=Part.find_all_by_vehicle_id(params[:vehicle])
    @grade=0
    @box=params[:box]
    @parts.each do |p|
      if p.component.name==params[:part]
        @grade=p.grade
      end

    end

    respond_to do |format|
      format.js
    end

  end


end
