class VehiclesController < ApplicationController
  # GET /vehicles
  # GET /vehicles.json
  def index
    @vehicles = Vehicle.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @vehicles }
    end
  end

  # GET /vehicles/1
  # GET /vehicles/1.json
  def show
    @vehicle = Vehicle.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @vehicle }
    end
  end

  # GET /vehicles/new
  # GET /vehicles/new.json
  def new

    @first = !params[:first].nil?
    @vehicle = Vehicle.new
    @modelos = Model.all
    @modelos.sort_by! {|m| m.brand}


   

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @vehicle }
    end
  end

  # GET /vehicles/1/edit
  def edit
    @vehicle = Vehicle.find(params[:id])
    @modelos = Model.all
    @only = Vehicle.where(:user_id => current_user.id).size ==1
  end



  def load_models
  @models = Model.find_all_by_brand(params[:brand])
  end

  # POST /vehicles
  # POST /vehicles.json
  def create
    @vehicle = Vehicle.new(params[:vehicle])
    @vehicle.user =current_user



    component_types=ComponentType.all

    partes_defecto=@vehicle.model.components

    component_types.each do |c|

        pieza_existe=@vehicle.model.components.find_by_component_type_id(c.id)


        if !pieza_existe.blank?
          #Existe una pieza por defecto para este vehiculo
          part_nueva=Part.new(:component=>pieza_existe, :vehicle=>@vehicle, :grade=>5, :creation_date=>Time.now)
          part_nueva.spoil_date = part_nueva.creation_date + estimate_lifetime(part_nueva.grade).days

        else
          #No existe una pieza por defecto, hay que crearla
          pieza_existe=Component.find_by_name(c.name)

          part_nueva=Part.new(:component=>pieza_existe, :vehicle=>@vehicle, :grade=>5, :creation_date=>Time.now)
          part_nueva.spoil_date = part_nueva.creation_date + estimate_lifetime(part_nueva.grade).days

        end

        part_nueva.save

    end



    #Temporal, hasta que se implemente subir archivos:
    @vehicle.picture = "impreza.jpg"

    respond_to do |format|
      if @vehicle.save
        format.html { redirect_to client_home_path({:id =>current_user.id,  :vehicle => @vehicle})}
        format.json { render json: @vehicle, status: :created, location: @vehicle }
      else
        @modelos=Model.all
        format.html { render action: "new" }
        format.json { render json: @vehicle.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /vehicles/1
  # PUT /vehicles/1.json
  def update
    @vehicle = Vehicle.find(params[:id])

    respond_to do |format|
      if @vehicle.update_attributes(params[:vehicle])
        format.html { redirect_to client_home_path({:id =>current_user.id,  :vehicle => @vehicle})}
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @vehicle.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vehicles/1
  # DELETE /vehicles/1.json
  def destroy



    @vehicle = Vehicle.find(params[:id])
    @vehicle.destroy


    respond_to do |format|
      format.html {redirect_to client_home_path(current_user.id)}

      format.json { head :no_content }
    end
  end

  def get_vehicle_parts
    @vehicle = Vehicle.find(params[:id])
    @parts=@vehicle.parts
    @parts.sort_by! {|p| p.grade}
    respond_to do |format|
      format.json { render json: @parts }
    end

  end

  def get_popular_models
       @models=Model.find_all_by_brand(params[:brand]).sort_by! {|m| Vehicle.find_all_by_model_id(m.id).count}

       @models.select! {|m| Vehicle.find_all_by_model_id(m.id).count>0}

       respond_to do |format|
         format.json { render json: @models }
       end

  end



end
