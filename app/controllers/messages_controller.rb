class MessagesController < ApplicationController

  def index
    @user = current_user
    @contact = params[:contact].nil? ? nil : User.find(params[:contact])
    @new = !params[:new].nil?

    #Si no selecciono ningun historial (y no hay 0), se mostrará el más reciente
    if !@new && @contact.nil? && !@user.contacts.blank?
      @contact = @user.contacts.last
    end


    #Leemos todos los mensajes
    if !@contact.nil? && !Message.find_all_by_sender_id_and_receiver_id(@contact.id,@user.id).blank?
      Message.find_all_by_sender_id_and_receiver_id(@contact.id,@user.id).each do|m|
        m.read_it
      end
    end
     h=234234
  end


  def create_history

    @user = User.find(params[:from])
    @contact = User.find(params[:to])
    @message = Message.create(:sender_id => @user.id, :receiver_id => @contact.id, :text => params[:text], :date => Time.now-4.hours, :read => false )


    render "create_from_history"

  end

  def create_compose

    recipient_data = params[:message_compose_receiver]
    text_data = params[:message_compose_text_area]

    @user = current_user
    @contact = User.all.select {|u| u.full_name == recipient_data}.first
    @message = Message.create(:sender_id => @user.id, :receiver_id => @contact.id, :text => text_data, :date => Time.now-4.hours, :read => false )

    redirect_to  messages_path(:contact => @contact.id)

  end


end