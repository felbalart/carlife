class PartsController < ApplicationController
  # GET /Parts
  # GET /Parts.json
  def index
    @parts = Part.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @parts }
    end
  end

  # GET /Parts/1
  # GET /Parts/1.json
  def show
    @part = Part.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @part }
    end
  end

  # GET /Parts/new
  # GET /Parts/new.json
  def new
    @part = Part.new

    if !owns_vehicle(params[:vehicle])
    redirect_to client_home_path(current_user)
    return
    end

    @vehicle = Vehicle.find(params[:vehicle])

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @part }
    end
  end

  # GET /Parts/1/edit
  def edit
    @part = Part.find(params[:id])
    @vehicle = @part.vehicle
  end

  # POST /Parts
  # POST /Parts.json
  def create

    @part = Part.new(params[:part])

    @part.spoil_date = @part.creation_date + estimate_lifetime(@part.grade).days

    respond_to do |format|
      if @part.save
        format.html { redirect_to client_home_path({:id =>current_user.id,  :vehicle => @part.vehicle, :parts => ""})}
        format.json { render json: @part, status: :created, location: @part }
      else
        format.html { render action: "new" }
        format.json { render json: @part.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /Parts/1
  # PUT /Parts/1.json
  def update
    @part = Part.find(params[:id])

    respond_to do |format|
      if @part.update_attributes(params[:part])
        format.html { redirect_to client_home_path({:id =>current_user.id,  :vehicle => @part.vehicle, :parts => ""})}
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @part.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /Parts/1
  # DELETE /Parts/1.json
  def destroy
    @part = Part.find(params[:id])
    vehicle = @part.vehicle
    @part.destroy


    if !owns_vehicle(@part.vehicle)
      redirect_to client_home_path(current_user)
      return
    end

    respond_to do |format|
      format.html { redirect_to client_home_path({:id =>current_user.id,  :vehicle => vehicle, :parts => ""})}
      format.json { head :no_content }
    end
  end

end


