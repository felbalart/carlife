class MaintenanceIssuesController < ApplicationController
  # GET /maintenance_issues
  # GET /maintenance_issues.json
  def index
    @maintenance_issues = MaintenanceIssue.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @maintenance_issues }
    end
  end

  # GET /maintenance_issues/1
  # GET /maintenance_issues/1.json
  def show
    @maintenance_issue = MaintenanceIssue.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @maintenance_issue }
    end
  end

  # GET /maintenance_issues/new
  # GET /maintenance_issues/new.json
  def new
    @maintenance_issue = MaintenanceIssue.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @maintenance_issue }
    end
  end

  # GET /maintenance_issues/1/edit
  def edit
    @maintenance_issue = MaintenanceIssue.find(params[:id])
  end

  # POST /maintenance_issues
  # POST /maintenance_issues.json
  def create
    @maintenance_issue = MaintenanceIssue.new(params[:maintenance_issue])

    respond_to do |format|
      if @maintenance_issue.save
        format.html { redirect_to @maintenance_issue, notice: 'Maintenance issue was successfully created.' }
        format.js
        format.json { render json: @maintenance_issue, status: :created, location: @maintenance_issue }
      else
        format.html { render action: "new" }
        format.json { render json: @maintenance_issue.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /maintenance_issues/1
  # PUT /maintenance_issues/1.json
  def update
    @maintenance_issue = MaintenanceIssue.find(params[:id])

    respond_to do |format|
      if @maintenance_issue.update_attributes(params[:maintenance_issue])
        format.html { redirect_to @maintenance_issue, notice: 'Maintenance issue was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @maintenance_issue.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /maintenance_issues/1
  # DELETE /maintenance_issues/1.json
  def destroy
    @maintenance_issue = MaintenanceIssue.find(params[:id])
    @maintenance_issue.destroy

    respond_to do |format|
      format.html { redirect_to maintenance_issues_url }
      format.json { head :no_content }
    end
  end
end
