class ProductsController < ApplicationController
  # GET /products
  # GET /products.json
  def index
    @products = Product.all
    @categories = Category.all
    @categories.sort_by! {|c| c.name}

    if params[:category].nil?
      @mensaje="Mostrando los productos para todas las categorias"
    else
      @products=Product.find_all_by_category_id(params[:category])
      @cat_aux=Category.find_by_id(params[:category])
      @mensaje="Mostrando los productos de la categoria " + @cat_aux.name
      if @products.count()==0
        @mensaje="Por el momento no hay productos para la categoria "+ @cat_aux.name
      end
    end




    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @products }
    end
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @product = Product.find(params[:id])
    @user=current_user

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @product }
    end
  end

  # GET /products/new
  # GET /products/new.json
  def new
    @product = Product.new
    @categorias=Category.all
    @categorias.sort_by! {|c| c.name}
    @user=current_user

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @product }
    end
  end

  # GET /products/1/edit
  def edit
    @product = Product.find(params[:id])
    @categorias=Category.all
    @user=current_user
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(params[:product])
    @product.supplier=current_user
    @categoria=Category.find_by_name(params[:selected_category])
    @product.category=@categoria
	
	@categorias=Category.all
	@user=current_user
	
    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render json: @product, status: :created, location: @product }
      else
        format.html { render action: "new" }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /products/1
  # PUT /products/1.json
  def update
    @product = Product.find(params[:id])
    @categoria=Category.find_by_name(params[:selected_category])
    @product.category=@categoria
	
	@categorias=Category.all
	@user=current_user
	
    respond_to do |format|
      if @product.update_attributes(params[:product])
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product = Product.find(params[:id])
    @product.destroy

    respond_to do |format|
      format.html { redirect_to "/suppliers/"+current_user.id.to_s }
      format.json { head :no_content }
    end
  end


  def search
    @products = Product.search(params[:search])
    @mensaje="Resultados de la busqueda"
  end

end
