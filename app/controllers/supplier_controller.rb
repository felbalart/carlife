class SupplierController < ApplicationController
  def index
    @supplier = Supplier.find(params[:id])
    @products = Product.find_all_by_supplier_id(@supplier.id)
  end

  def create
    @supplier =  Supplier.new(params[:supplier])
    @supplier.type = "Supplier"


    respond_to do |format|
      if @supplier.save
        session[:user_id] = @supplier.id
        format.html { redirect_to "/suppliers/"+@supplier.id.to_s }
        format.json { render json: @supplier, status: :created, location: @supplier }
      else
        format.html { render action: "new" }
        format.json { render json: @supplier.errors, status: :unprocessable_entity }
      end
    end
  end

  def new
    @supplier = Supplier.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @supplier }
    end
  end

end
