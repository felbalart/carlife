
function filter_new_event_fields()
{
      var selected = "."+ $('#event_type').find(":selected").val();
      $(".event_field").hide();

      $(selected).show();

}

function redraw_new_event_headers()
{
    var selected_value = $('#event_type').find(":selected").val();
    var selected_text = $('#event_type').find(":selected").text();
   $("#new_event_picture").attr("src", "/assets/event_icons/EventIcon-"+selected_value+".png");
   $("#new_event_title").html("Nuevo Evento: "+selected_text)
}

