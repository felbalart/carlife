function fb_test(){
  console.debug("Function was called correctly!")
}


function fb_sdk()
{


  // Additional JS functions here
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '442749189153868', // App ID
      channelUrl : '//carlifeuc.herokuapp.com/channel.html', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });

    // Additional init code here

  };

  // Load the SDK asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));

}


function fb_ajax(action)
{
      //Existen 2 acciones: signup y login
      console.debug("Fb Ajax Function called!")

      FB.api('/me', function(response) {

        var fname = response.first_name;
        var lname = response.last_name;
        console.debug("Fb Api resp!")

        $.ajax({
          type: "POST",
          url: '/'+action+'/fb',
          data: { username: response.username, first_name: response.first_name,  last_name: response.last_name},
          success:  function(data){
              console.debug("Recibimos la respuesta:");
              console.debug(data);
              

              if(data=="signup_accepted")
              {window.location = '/vehicles/new?first=';
              console.debug(111)
              }
              else if(data=="signup_existing")   
              {  console.debug(22222)
                if(confirm('Usuario '+fname+' '+lname+' ya está registrado en CarLife\nDebe cerrar sesión para registrar usuario nuevo\n¿Desea Continuar?')) 
                    FB.logout(function(response) {
                    $(".fb-login-button").click()
                    });
              }
              else if(data=="login_accepted")   
              {  console.debug(33);
                 window.location = '/'

              }
              else if(data=="login_unexisting")   
              {  console.debug(4444);

                 if(confirm('El usuario actúal de Facebook  ('+fname+' '+lname+') no está registrado en CarLife\n\n¿Desea Registrarse?')) 
                   {
                    window.location = '/clients/new';
                   }
                else
                 FB.logout(function(response) {
                    $(".fb-login-button").click()
                    });                 


              }





          },
          dataType: "text"

        });

       console.debug("Tb: post post")
      });

}


function fb_signup(){


FB.login(function(response) { vladi4 = response;
    if (response.authResponse) {
        {console.debug("The person logged into your app");
        fb_ajax("signup");
        }

    } else {
        console.debug(" The person cancelled the login dialog");
    }
});

}





function fb_login(){

  FB.login(function(response) {
    if (response.authResponse) {
        {console.debug("The person logged into your app");
        fb_ajax("login");
        }

    } else {
        console.debug(" The person cancelled the login dialog");
    }
});



}
