


function paint_stars(nstars)
{
    for (var i=1;i<=nstars ;i++)
    {$("#star_"+i).attr("src", "/assets/star.png");}

    for (var j=nstars+1;j<=5 ;j++)
    {   $("#star_"+j).attr("src", "/assets/wstar.png");}

}


function load_stars()
{   var s = parseInt($("#parts_grade_hidden_field").val());
    paint_stars(s);

}

function update_grade(s)
{
    $("#parts_grade_hidden_field").val(s);
}

function explain_state(prom){

    if($(".vehicle_state").find(".state_explanation").length>=1){
        $(".state_explanation").remove();
    }
    else{
    if(prom==1){
    $(".vehicle_state").append('<div class="state_explanation">Tu vehículo está en estado crítico. La mayoría de sus partes se encuentran en pésimas condiciones, lo que podría causar una falla grave o peor aún, un accidente. Deberías contactar inmediatamente a un proveedor hacer una mantención profunda a sus componentes principales.<div>');
    }
    else if (prom==2){
    $(".vehicle_state").append('<div class="state_explanation">Tu vehículo está en mal estado. Varias partes necesitan de una mantención, por lo que es muy recomendable que contactes a un proveedor para hacerlo. Revisa aquí abajo cuales de ellas necesitan más atención.<div>');
    }
    else if (prom==3){
    $(".vehicle_state").append('<div class="state_explanation">Tu vehículo se encuentra en condiciones regulares. Varios componentes no se encuentran en estado óptimo, por lo que te recomendamos realizar una mantención con algún proveedor.<div>');
    }
    else if (prom==4){
    $(".vehicle_state").append('<div class="state_explanation">Tu vehículo se encuentra en buen estado. Algunos componentes necesitan una mejora, por lo que podría ser recomendable contactar a un prooveedor para realizar una mantención.<div>');
    }
    else{
    $(".vehicle_state").append('<div class="state_explanation">Tu vehículo se encuentra en perfectas condiciones. La mayoría de sus componentes se encuentran en estado óptimo para operar.<div>');
    }
    }


}