class Event < ActiveRecord::Base
belongs_to :component
belongs_to :vehicle

def self.type_options
  [RefuelEvent,TripEvent,InactivityEvent,ComponentFailureEvent,ComponentFixEvent,ComponentInstallEvent,CrashEvent,GenericEvent]
end

def self.fantasy_name
  case self.to_s
    when "RefuelEvent"
      "Carga de Combustible"
    when "TripEvent"
      "Viaje"
    when "InactivityEvent"
      "Periodo de Inactividad"
    when "ComponentFailureEvent"
      "Falla"
    when "ComponentFixEvent"
      "Reparacion"
    when "ComponentInstallEvent"
      "Recambio de pieza"
    when "CrashEvent"
      "Choque"
    when "GenericEvent"
      "Otro"
    else
      "els"
  end


end

end