class Vehicle < ActiveRecord::Base
belongs_to :model
belongs_to :user
has_many :parts, :dependent => :delete_all
has_many :events
has_many :maintenances

  validates :color, :year, :model_id, :user_id, presence:true

  def full_name
    self.model.brand+" "+self.model.name+" "+self.year.to_s
  end

  def total_costs
    costs = 0
    self.parts.select{|pa| !pa.price.nil?}.each do |p|
      costs = costs+p.price
    end

    self.maintenances.select{|ma| !ma.price.nil?}.each do |m|
      costs = costs+m.price
    end

    return costs
  end

  def grade
     parts=Part.find_all_by_vehicle_id(self.id)
     grade=0
     parts.each do |p|
       grade=grade+p.grade
     end

     if parts.count!=0
     grade=grade/parts.count
     else grade=0
     end

     return grade
  end

end
