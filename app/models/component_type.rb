class ComponentType < ActiveRecord::Base

  has_many :components
  has_many :grade_descriptions

end
