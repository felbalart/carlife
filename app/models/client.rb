# encoding: UTF-8
class Client < User
  has_many :vehicles

  validates :email, :password, :first_name, :last_name, :birth_date, :type, presence:true
  validates :email, uniqueness: true, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create }


  def vehicles
    Vehicle.find_all_by_user_id(self.id)
  end


  def full_name
    self.first_name+" "+self.last_name
  end

  def parts_warn

    deficient_parts = self.vehicles.map{|v| v.parts}.flatten.select{|p| p.grade <=1}

    if deficient_parts.blank?
      return
    end

    last_warning_message = Admin.first.messages_to(self).select{|m| m.text.starts_with? 'Alerta de componentes defectuosos:'}.sort_by(&:date).last

    #Si tiene partes malas y no le hemos avisado, o le avisamos hace mas de 1 mes
    if !deficient_parts.blank? && (last_warning_message.blank? || ((last_warning_message.date + 1.month) < Time.now) )
      warning_text = "Alerta de componentes defectuosos:\n\nEstimado #{self.full_name},\n\nLe informamos que detectamos los siguientes componentes defectuosos en sus vehículos:\n\n"

      deficient_parts.each do |dp|
        warning_text += "Vehiculo: #{dp.vehicle.full_name}, Componente: #{dp.component.name}, Estado: #{dp.grade}/5\n"
      end

      warning_text += "\nLe sugerimos encarecidamente que someta su(s) vehículo(s) a una mantención, para lo cúal lo invitamos a revisar las ofertas disponibles en nuestro sitio"

      Message.create(:sender_id => Admin.first.id, :receiver_id => self.id, :text => warning_text, :date => Time.now-4.hours, :read => false )
    else
      logger.warn 333
    end


  end


end
