class Supplier < User

  validates :email, :password, :name, :address, :website, :type, presence:true
  validates :email, uniqueness: true, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create }

  has_many :products

  def full_name
    self.name
  end

end
