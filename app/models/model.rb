class Model < ActiveRecord::Base

  has_and_belongs_to_many :components

  validates :name, :brand, presence:true

end
