class Maintenance < ActiveRecord::Base

  belongs_to :vehicle
  has_many :maintenance_issues





  validates :date, :vehicle_id, presence:true

  validates :fixed_parts, :changed_parts, :numericality => { :only_integer => true, :greater_than_or_equal_to => 0 }

end
