class Admin < User

  validates :email, :password, presence:true

  def full_name
    "Carlife Webmaster"
  end

end
