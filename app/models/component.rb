class Component < ActiveRecord::Base

  has_many :parts
  has_many :events
  belongs_to :component_type
  has_and_belongs_to_many :models

end
