class Product < ActiveRecord::Base

  validates :name, :description, :price, presence:true
  validates_numericality_of :price, :only_integer => true

  belongs_to :supplier
  belongs_to :category


  def self.search(search)
    search_condition = "%" + search + "%"
    #products=find(:all, :conditions => ['name LIKE ? OR description LIKE ? OR price LIKE ?', search_condition, search_condition, search_condition ])
    products=Product.where("name LIKE ? OR description LIKE ? OR price LIKE ?", search_condition, search_condition, search_condition).all
    proveedor=Supplier.where("name LIKE ?", search_condition).all

    proveedor.each do |prov|

      prov.products.each do |p|
          products<<p
        end
    end



    return products


  end

end
