# encoding: UTF-8
class Part < ActiveRecord::Base
  belongs_to :vehicle
  belongs_to :component

  has_many :maintenances


  def remaining_lifetime
    days = ((self.spoil_date - Time.now).to_f/86400).to_i



    if days.abs<=60
      days.to_s+" días"
    elsif days.abs<730
      (days/30).to_s+" meses"

    else
      (days/365).to_s+" años"
    end


  end

  def component_name
    return component.name
  end
end
