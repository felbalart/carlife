class User < ActiveRecord::Base
  has_many :vehicles


  def contacts
      self.messages.map{ |m| m.other_user(self)}.uniq
  end

  def messages
    (Message.find_all_by_sender_id(self.id)+ Message.find_all_by_receiver_id(self.id)).sort_by(&:date)
  end

  def messages_with(user)
    Message.between(self,user)
  end

  def messages_to(user)
    Message.find_all_by_sender_id_and_receiver_id(self.id,user.id)
  end

  def messages_from(user)
    Message.find_all_by_sender_id_and_receiver_id(user.id,self.id)
  end

  def unread_count
    Message.find_all_by_receiver_id(self.id).select{|m| !m.read}.size
  end


end
