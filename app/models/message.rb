class Message < ActiveRecord::Base

  def sender
    User.find(self.sender_id)
  end

  def receiver
    User.find(self.receiver_id)
  end

  def self.between (user1, user2)
    (Message.find_all_by_sender_id_and_receiver_id(user1.id,user2.id) + Message.find_all_by_sender_id_and_receiver_id(user2.id,user1.id)).sort_by(&:date)
  end


  def other_user(user)
    self.sender ==  user ? self.receiver : self.sender
  end

  def read_it
    if !self.read
      self.read = true
      self.save
    end
  end

end
