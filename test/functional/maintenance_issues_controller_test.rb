require 'test_helper'

class MaintenanceIssuesControllerTest < ActionController::TestCase
  setup do
    @maintenance_issue = maintenance_issues(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:maintenance_issues)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create maintenance_issue" do
    assert_difference('MaintenanceIssue.count') do
      post :create, maintenance_issue: @maintenance_issue.attributes
    end

    assert_redirected_to maintenance_issue_path(assigns(:maintenance_issue))
  end

  test "should show maintenance_issue" do
    get :show, id: @maintenance_issue
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @maintenance_issue
    assert_response :success
  end

  test "should update maintenance_issue" do
    put :update, id: @maintenance_issue, maintenance_issue: @maintenance_issue.attributes
    assert_redirected_to maintenance_issue_path(assigns(:maintenance_issue))
  end

  test "should destroy maintenance_issue" do
    assert_difference('MaintenanceIssue.count', -1) do
      delete :destroy, id: @maintenance_issue
    end

    assert_redirected_to maintenance_issues_path
  end
end
