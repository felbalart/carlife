class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :title
      t.string :picture
      t.text :abstract
      t.text :middle
      t.text :body

      t.timestamps
    end
  end
end
