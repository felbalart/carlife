class CreateComponentsModelsTable < ActiveRecord::Migration
  def self.up
    create_table :components_models, :id => false do |t|
        t.references :component
        t.references :model
    end
    add_index :components_models, [:component_id, :model_id]
    add_index :components_models, [:model_id, :component_id]
  end

  def self.down
    drop_table :components_models
  end
end