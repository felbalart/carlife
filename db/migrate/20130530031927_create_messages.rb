class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.text :text
      t.datetime :date
      t.integer :sender_id
      t.integer :reciever_id
      t.boolean :read

      t.timestamps
    end
  end
end
