class RenamePieceToPart < ActiveRecord::Migration
  def self.up
    rename_table :pieces, :parts
  end

  def self.down
    rename_table :parts,:pieces
  end
end