class CreateComponents < ActiveRecord::Migration
  def change
    create_table :components do |t|
      t.integer :component_type_id
      t.string :name

      t.timestamps
    end
  end
end
