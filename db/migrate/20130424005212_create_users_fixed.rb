class CreateUsersFixed < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :password
      t.string :first_name
      t.string :last_name
      t.date   :birth_date
      t.string :name
      t.string :address
      t.string :website
      t.string :type

      t.timestamps
    end
  end
end
