class CreateTableProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :description
      t.integer :price
      t.integer :supplier_id
      t.string  :category_id

      t.timestamps
    end

  end
end
