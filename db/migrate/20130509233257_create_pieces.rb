class CreatePieces < ActiveRecord::Migration
  def change
    create_table :pieces do |t|
      t.integer :component_id
      t.datetime :creation_date
      t.datetime :spoil_date
      t.integer :grade

      t.timestamps
    end
  end
end
