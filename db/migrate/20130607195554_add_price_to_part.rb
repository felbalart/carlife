class AddPriceToPart < ActiveRecord::Migration
  def change
    add_column :parts, :price, :integer
  end
end
