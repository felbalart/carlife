class FixModels < ActiveRecord::Migration
  def change
    remove_column :models,:type
    remove_column :models,:string

    add_column :models, :kind,:string
  end
end
