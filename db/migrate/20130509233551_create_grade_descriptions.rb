class CreateGradeDescriptions < ActiveRecord::Migration
  def change
    create_table :grade_descriptions do |t|
      t.integer :component_type_id
      t.integer :grade
      t.string :text

      t.timestamps
    end
  end
end
