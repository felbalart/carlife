class CreateProducts < ActiveRecord::Migration
  def change

    drop_table :products

    create_table :products do |t|

      t.string :name
      t.string :description
      t.integer :price
      t.integer :supplier_id
      t.string  :category_id


      t.timestamps
    end
  end
end
