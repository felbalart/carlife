class CreateMaintenances < ActiveRecord::Migration
  def up
    create_table :maintenances do |t|
      t.date :date
      t.integer :vehicle_id
      t.integer :fixed_parts
      t.integer :changed_parts
      t.string :supplier
      t.string :description

      t.timestamps
    end
  end

  def down
    drop_table :maintenances
  end

end
