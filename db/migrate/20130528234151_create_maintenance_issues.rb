class CreateMaintenanceIssues < ActiveRecord::Migration
  def up
    create_table :maintenance_issues do |t|
      t.integer :maintenance_id
      t.integer :part_id
      t.boolean :fixed
      t.integer :old_grade
      t.integer :new_grade
      t.integer :new_part_id

      t.timestamps
    end
  end

  def down
    drop_table :maintenance_issues
  end
end
