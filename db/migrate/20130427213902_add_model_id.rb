class AddModelId < ActiveRecord::Migration
  def change
    add_column :vehicles, :model_id, :integer
    add_column :users, :vehicle_id, :integer
  end
end
