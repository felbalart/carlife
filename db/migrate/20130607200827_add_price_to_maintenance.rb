class AddPriceToMaintenance < ActiveRecord::Migration
  def change
    add_column :maintenances, :price, :integer
  end
end
