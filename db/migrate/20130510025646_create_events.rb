class CreateEvents < ActiveRecord::Migration
  def change
  		create_table :events do |t|
  			t.integer :vehicle_id
  			t.string :type
  			t.integer :component_id
  			t.string :description
  			t.string :destiny
  			t.integer :km
  			t.string :place
  			t.timestamp :time
  			t.integer :litres

  			t.timestamps
  		end
	end
end
