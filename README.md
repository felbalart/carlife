CarLife UC
==============

URL Heroku: http://carlifeuc.herokuapp.com

Entrega 5:

Alfredo, las direcciones de las APIs son:

1) Obtener todas las partes de un veh�culo, ordenadas por su estado:
api/parts/<id vehiculo>.json

2) Obtiene todos los modelos de una marca, ordenados por popularidad:
api/popular/<nombre de la marca>.json

3) Obtiene todos los vehiculos de un usuario, cada uno con sus detalles, sus partes (como objetos hijos) y los eventos asociados al vehiculo (tambien como hijos)
api/vehicle/<id usuario>.json